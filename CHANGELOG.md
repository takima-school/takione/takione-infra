# What's new

* [step-1] [Terraform] Le boilerplate pour le début du live-coding terraform
* [step-2] [Terraform][CI] Le terraform qui lance un EKS prêt à l'emploi
* [step-3] [Ansible] Le boilerplate pour le début du live-coding ansible
* [step-4] [Ansible][CI] Le ansible qui provisionne le cluster
* [step-final] [Terraform][Ansible][CI] provisionning de l'infra complète